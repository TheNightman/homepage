# Homepage - J Diggity

This repo contains the code for my personal website, soon to be hosted at [jdiggity.me](http://jdiggity.me/).

# Running
 * `. venv/bin/activate`
 * `python -m flask run`