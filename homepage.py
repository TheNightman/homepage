from flask import Flask, render_template, send_from_directory
from flask_appconfig import AppConfig
from flask_bootstrap import Bootstrap

from nav import nav

app = Flask(__name__)
AppConfig(app)
Bootstrap(app)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
nav.init_app(app)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/jaytoo')
@app.route('/j2')
@app.route('/J2')
def jaytoo():
    return render_template('jaytoo.html', title='Jaytoo')

@app.route('/jaytoo/jaytoo-live.iso')
def serve_jaytoo():
    return send_from_directory('static/jaytoo', 'jaytoo-latest.iso')
